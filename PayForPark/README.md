# Fangzheng Li - CS 445-Final Porject 11/13/2018



In this project I use Maven, Jacoco, Spring boot.


## Configuration Instructions

1. openjdk version "1.8.0_131"
2. maven


## To Run the program
1. Build maven project and run test
2. Use jacoco to generate coverage
3. Open test report with a browser, this report is found in 'target/site/jacoco/index.html'
4. Run the application using Spring Boot with comand 'mvn spring-boot:run'

## Credits and acknowledgements

Coded by Fangzheng Li


