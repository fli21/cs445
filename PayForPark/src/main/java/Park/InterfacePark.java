package Park;

import org.json.JSONObject;

public interface InterfacePark {
    JSONObject viewInformation();
    int getParkId();

}
